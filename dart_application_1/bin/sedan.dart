import 'CarComponent.dart';
import 'Runable.dart';


class sedan extends CarComponent implements Runable{
  String name = '';
  String color = '';
  sedan(String name, String color) : super('Sedan', 'White', 4, 100, 4) {
    this.name = name;
    this.color = color;
  }

  @override
  void brake() => print("Sedan : $name color : $color is applyBreak rightnow");

  @override
  void run() =>
      print('Sedan : $name color : $color is runing with $speed km/hr');

  @override
  void shutdown() => print('Sedan : $name color : $color is shuting dowm');

  @override
  void start() {
    print('Sedan : " + $name  "color : "  $color  " is Starting');
  }

  @override
  void speedUp() {
    print('Sedan : "  $name  "color : "  $color  " is raising speed');
  }
}