import 'CarComponent.dart';
import 'LoadCargo.dart';
import 'Runable.dart';

class PickUpTruck extends CarComponent implements Runable ,LoadCargo {
  String name = '';
  String color = '';

  PickUpTruck(String name, String color) : super('pickup', 'White', 4, 100, 5) {
    this.name = name;
    this.color = color;
  }

  @override
  void brake() => print("Pickup : $name color : $color is applyBreak rightnow");

  @override
  void run() =>
      print('Pickup : $name color : $color is runing with $speed km/hr');

  @override
  void shutdown() => print('Pickup : $name color : $color is shuting dowm');

  @override
  void start() {
    print('Pickup : "  $name  "color : "  $color  " is Starting');
  }

  @override
  void speedUp() {
    print('Pickup : "  $name  "color : "  $color  " is raising speed');
  }
  
  @override
  void loading() {
    print('Pickup : "  $name  "color : "  $color  " is loading cargo');
  }
}
