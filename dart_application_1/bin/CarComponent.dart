
abstract class CarComponent {
  late String name;
  late String color;
  late int numOfDoor;
  late int speed;
  late int numOfSeats;

  CarComponent(
      this.name, this.color, this.numOfDoor, this.speed, this.numOfSeats);
  String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }

  String getColor() {
    return color;
  }

  void setColor(String color) {
    this.color = color;
  }

  int getNumOfDoor() {
    return numOfDoor;
  }

  void setNumOfDoor(int numOfDoor) {
    this.numOfDoor = numOfDoor;
  }

  int getSpeed() {
    return speed;
  }

  void setSpeed(int speed) {
    this.speed = speed;
  }

  int getNumOfSeats() {
    return numOfSeats;
  }

  void setNumOfSeats(int numOfSeats) {
    this.numOfSeats = numOfSeats;
  }

  void start();
  void speedUp();
  void brake();
  void shutdown();
}
