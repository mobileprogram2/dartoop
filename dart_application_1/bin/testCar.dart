import 'dart:io';

import 'pickUpTruck.dart';
import 'sedan.dart';

void main(){
var p1 = PickUpTruck('Triton', 'black');
p1.start();
p1.run();
p1.speedUp();
p1.brake();
p1.shutdown();
p1.loading();
print(' ');
print('---------------------------');
print(' ');

var s1 = sedan('mirage', 'green');
s1.start();
s1.run();
s1.speedUp();
s1.brake();
s1.shutdown();
}